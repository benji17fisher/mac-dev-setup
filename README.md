# Configure a Mac with Ansible

## Overview

[Ansible](http://www.ansible.com/home) is a configuration-management tool,
often used for managing servers. This project uses Ansible to install and
configure software on Mac in order to easily and reliably set up a development
environment. I encourage pull requests from other NorthPoint developers.

## Prerequisites

There are two steps that we cannot automate with this project:

1. Upgrade OS X to the current version through the App Store.
2. Install Xcode through the App Store.

Make sure you have a good internet connection. Use ethernet (wired) instead of
wifi if you can.

## Real developers do not read documentation!

1. Download a copy of this repository.
2. Open a Terminal window.
3. Change to the main directory of this project (the directory containing this
   README).
4. `$ ./bootstrap.sh`
5. Follow the prompts.

## Just kidding:  tell me what it does!

The script installs `pip` (the Python package manager) and uses that to
install `ansible`. Then Ansible does all the real work, with a lot of help from
other package managers, such as [Homebrew](http://brew.sh/). For more details,
read "What gets installed", below.

If you want more control over what gets installed, then read the following
section.  You can modify what the script does by editing a few plain-text
files.

## Running the playbook manually

Once you have the prerequisites, edit the variables in `vars/main.yml` and
comment out any roles in `playbook.yml` that you do not want installed.
Then, from the directory containing this README,

    $ ansible-playbook --ask-sudo-pass -i hosts playbook.yml

(You can use `-K`, the short form of `--ask-sudo-pass`, if you prefer.)  When
you get the prompt `sudo password:` enter your password.

The next section details what gets installed for each selected role.

Some roles are prerequisites for others, so they may be included even if you
edit them out in `playbook.yml`.

## What gets installed

Most command-line utilities are installed using [Homebrew](http://brew.sh/)
under `/usr/local/bin`.
Most GUI applications are installed using [Homebrew Cask](http://caskroom.io/)
under `~/Applications`.

The following package managers are installed even if you do not select any
roles:

- Homebrew
- Homebrew Cask
- npm (Node Package Manager) and node (a.k.a. node.js)
- bundler (for Ruby gems)

### Miscellaneous Command-Line Utilities (cli)

- wget
- ctags (Exuberant ctags)

### Drush (drush)

- drush
- PHP CodeSniffer and Drupal coding standards

### Git Configuration (git-config)

- SourceTree
- some basic Git configuration files

### Miscellaneous GUI Applications (gui-apps)

- Alfred
- Chrome
- Cyberduck
- MacGDBp

### PHP (php)

- PHP
- composer

### Sublime Edit (sublime)

- Sublime Edit 2

### Syntax Checkers (syntax-checkers)

- js-lint
- js-yaml
- csslint

### Vim (vim)

- MacVim (under `/Applications`)

### Vim Configuration for Drupal (vim-drupal)

- Vim configuration for editing Drupal:  the
  [Vimrc project](https://drupal.org/project/vimrc)

### Tools for Managing Virtual Machines (virtual-machines)

- Vagrant and Vagrant Bar
- VirtualBox
- Vagrant plugins:  vagrant-hostmanager

## Further thoughts and TODO list

I originally installed Homebrew first, then used it to install Ansible. Now
that is flipped, and Ansible is used to install Homebrew. This has not yet
been tested.

There must be a way to tell Homebrew to use the version of PHP already on
the system.  I should add a variable to specify which version of PHP to use.

I plan to add all of the local setup from the devstack-LAMP repo, converted to
Ansible.

Add version options for all installed packages.

Make sure that all tasks indicate ok/changed appropriately.

Add a role for PHPStorm.

Add more syntax checkers, and configure text editors (vim, sublime, PHP Storm)
to use them.

Reconsider how dotfiles are organized. Maybe create `~/.northpoint/` for
NP-standard dotfiles, then add `source` commands to the user's standard files.

Maybe change the quick-install instructions to something like this:

Open a Terminal window and enter these commands:

    $ git clone --recursive https://benji17fisher@bitbucket.org/benji17fisher/mac-dev-setup.git
    $ cd mac-dev-setup
    $ ./bootstrap.sh

and follow the prompts.

## Credits

The drush-specific steps are based on Jeff Geerling's
[drush role](https://github.com/geerlingguy/ansible-role-drush)
for Ansible (also available at
[Ansible Galaxy](https://galaxy.ansible.com/list#/roles/433)).


## Notes

Here are the manual steps I did before I executed this playbook. Once the
recommended steps have been tested, we can remove this section.

1. Upgrade OS X to the current version (10.9.5) through the App Store.
2. Install Xcode through the App Store.
3. Make sure the command-line tools (CLT) are installed.  As of OS X 10.9.5
   and Xcode 6.1, one way to do this is (thanks to
   [this issue](https://github.com/Homebrew/homebrew-php/issues/241)
   on the Homebrew-PHP issues queue)
   `$ xcode-select --install`
   and then confirm installation.
4. Install [Homebrew](http://brew.sh/)
5. Install the one program I *really* need:  `$ brew install macvim`
6. Install Ansible:  `$ brew install ansible`
7. Tell Ansible to save a log by adding `.ansible.cfg` in your home directory
   with the line
   `log_path = /usr/local/var/log/ansible.log`
