#!/usr/bin/env bash

###########################
### USAGE INFO
###########################

function HELP {
  echo '
USAGE

1. Run the script
2. Restart your shell
3. ???
4. Profit

Sets up a complete development environment using ansible, rbenv, pyenv, and ndenv.
'
  exit 1
}

###########################
### INSTALLATION FUNCTIONS
###########################

function install_command_line_tools {
  if pkgutil --pkg-info=com.apple.pkg.CLTools_Executables &> /dev/null; then
    return
  fi

  loud "Installing OS X Command Line Tools"

  # OS X Command Line Tools
  quiet "To install the OS X Command Line Tools, click on the install button on the dialogue that appears."
  echo -en "\tPress enter to continue... "
  read

  xcode-select --install

  echo -en "\tPress enter when the installation is complete. "
  read

  if pkgutil --pkg-info=com.apple.pkg.CLTools_Executables &> /dev/null; then
    quiet "Done installing OS X Command Line Tools."
  else
    loud "ERROR: Command line tools not found."
  fi
}

function install_pip {
  # Ensure pip is installed.
  if hash pip &> /dev/null; then
    return
  fi

  loud "Installing pip, the Python package manager."
  quiet "Installing pip: may require password."
  sudo easy_install pip &> /dev/null
}

function install_ansible {
  # Ensure ansible is installed.
  if hash ansible &> /dev/null; then
    return
  fi

  loud "Installing ansible, our configuration manager."
  quiet "Installing ansible: may require password."
  sudo pip install ansible &> /dev/null
}

function run_ansible {
  loud "Installing everything else with Ansible."
  quiet "Enter your password when prompted, then have some fun while this chugs along."
  ansible-playbook --ask-sudo-pass -i hosts playbook.yml
}

#####################
### HELPER FUNCTIONS
#####################

function loud {
  echo -e "\n#### ${1}"
}

function quiet {
  echo -e "\t${1}"
}

#####################
### MAIN FUNCTION
#####################

function main {
  install_command_line_tools
  install_pip
  install_ansible
  run_ansible

  loud 'Bootstrap Complete!'
  quiet 'Make sure you restart your shell. Now!\n'
}


################
### Parse Opts
################

while getopts ":hv" opt
do
  case $opt in
    h)
      echo -e "\nMac Dev Bootstrap Script
      $(HELP)
      "
      exit 0
      ;;
    \?)
      echo ""
      echo "Invalid option: -${OPTARG}" >&2
      HELP
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
   esac
done

main
